diff -Nurd freeciv/server/connecthand.c freeciv/server/connecthand.c
--- freeciv/server/connecthand.c	2024-04-12 07:19:30.221431641 +0300
+++ freeciv/server/connecthand.c	2024-04-12 07:19:45.477539308 +0300
@@ -18,6 +18,7 @@
 #include <string.h>
 
 /* utility */
+#include "astring.h"
 #include "capability.h"
 #include "fcintl.h"
 #include "log.h"
@@ -31,6 +32,7 @@
 #include "game.h"
 #include "packets.h"
 #include "player.h"
+#include "research.h"
 #include "version.h"
 
 /* server */
@@ -49,6 +51,7 @@
 #include "settings.h"
 #include "srv_main.h"
 #include "stdinhand.h"
+#include "techtools.h"
 #include "voting.h"
 
 /* server/scripting */
@@ -109,6 +112,34 @@
   conn_set_access(pconn, level, FALSE);
 }
 
+/************************************************************************//**
+  Give techs for the late joiners in freeciv-web longturn game.
+****************************************************************************/
+static void do_longturn_tech_latejoiner_effect(struct player *pplayer)
+{
+  struct research *presearch;
+  int mod = 35;
+  int num_players;
+
+  presearch = research_get(pplayer);
+  advance_index_iterate(A_FIRST, ptech) {
+    if (presearch != nullptr
+        && TECH_KNOWN == research_invention_state(presearch, ptech)) {
+      continue;
+    }
+
+    num_players = 0;
+    players_iterate(aplayer) {
+      if (TECH_KNOWN == research_invention_state(research_get(aplayer), ptech)) {
+        if (mod <= ++num_players) {
+          found_new_tech(presearch, ptech, FALSE, TRUE);
+          break;
+        }
+      }
+    } players_iterate_end;
+  } advance_index_iterate_end;
+}
+
 /**********************************************************************//**
   This is used when a new player joins a server, before the game
   has started. If pconn is nullptr, is an AI, else a client.
@@ -295,6 +326,25 @@
     notify_conn(dest, nullptr, E_CONNECTION, ftc_server,
                 _("You are logged in as '%s' connected to no player."),
                 pconn->username);
+
+    if (is_longturn()) {
+      pplayer = find_uncontrolled_player();
+      if (pplayer) {
+        /* Make it human! */
+        set_as_human(pplayer);
+        pplayer->economic.gold += game.info.turn * 10;
+        if (pplayer->economic.gold > 700) {
+          pplayer->economic.gold = 700;
+        }
+        pplayer->economic.science = 60;
+        pplayer->economic.tax = 40;
+        connection_attach(pconn, pplayer, FALSE);
+        do_longturn_tech_latejoiner_effect(pplayer);
+      } else {
+        notify_conn(dest, nullptr, E_CONNECTION, ftc_server,
+           _("Unable to join LongTurn game. The game is probably full."));
+      }
+    }
   } else {
     notify_conn(dest, nullptr, E_CONNECTION, ftc_server,
                 _("You are logged in as '%s' connected to %s."),
@@ -618,8 +668,18 @@
 struct player *find_uncontrolled_player(void)
 {
   players_iterate(played) {
-    if (!played->is_connected && !played->was_created) {
-      return played;
+    if (!is_longturn()) {
+      if (!played->is_connected && !played->was_created) {
+        return played;
+      }
+    } else {
+      if (((!played->is_connected && !played->was_created
+            && played->unassigned_user && played->is_alive)
+           || (played->is_alive && played->nturns_idle > 12 ))
+          && !player_delegation_active(played)
+          && played->server.delegate_to[0] == '\0') {
+        return played;
+      }
     }
   } players_iterate_end;
 
@@ -693,6 +753,9 @@
       }
       (void) aifill(game.info.aifill);
     }
+    if (is_longturn()) {
+      server_player_set_name(pplayer, pconn->username);
+    }
 
     if (game.server.auto_ai_toggle && !is_human(pplayer)) {
       toggle_ai_player_direct(nullptr, pplayer);
diff -Nurd freeciv/server/settings.c freeciv/server/settings.c
--- freeciv/server/settings.c	2024-04-12 07:19:30.225431668 +0300
+++ freeciv/server/settings.c	2024-04-12 07:19:45.481539337 +0300
@@ -858,6 +858,10 @@
 ****************************************************************************/
 static void metamessage_action(const struct setting *pset)
 {
+  if (is_longturn() && S_S_RUNNING == server_state()) {
+    return;
+  }
+
   /* Set the metaserver message based on the new meta server user message.
    * An empty user metaserver message results in an automatic meta message.
    * A non empty user meta message results in the user meta message. */
diff -Nurd freeciv/server/srv_main.c freeciv/server/srv_main.c
--- freeciv/server/srv_main.c	2024-04-12 07:19:30.221431641 +0300
+++ freeciv/server/srv_main.c	2024-04-12 07:19:45.481539337 +0300
@@ -3470,6 +3470,15 @@
         map_show_all(pplayer);
       } players_iterate_end;
     }
+
+    if (is_longturn()) {
+      players_iterate(pplayer) {
+        if (is_ai(pplayer)) {
+          set_as_human(pplayer);
+          server_player_set_name(pplayer, "New Available Player");
+	}
+      } players_iterate_end;
+    }
   }
 
   if (game.scenario.is_scenario && game.scenario.players) {
@@ -3935,3 +3944,11 @@
     game.server.world_peace_start = game.info.turn;
   }
 }
+
+/**********************************************************************//**
+ Is this a LongTurn game?
+**************************************************************************/
+bool is_longturn(void)
+{
+  return (!fc_strcasecmp(game.server.meta_info.type, "longturn"));
+}
diff -Nurd freeciv/server/srv_main.h freeciv/server/srv_main.h
--- freeciv/server/srv_main.h	2024-04-12 07:19:30.225431668 +0300
+++ freeciv/server/srv_main.h	2024-04-12 07:19:45.481539337 +0300
@@ -96,6 +96,7 @@
 void fc__noreturn srv_main(void);
 void fc__noreturn server_quit(void);
 void save_game_auto(const char *save_reason, enum autosave_type type);
+bool is_longturn(void);
 
 enum server_states server_state(void);
 void set_server_state(enum server_states newstate);
diff -Nurd freeciv/server/stdinhand.c freeciv/server/stdinhand.c
--- freeciv/server/stdinhand.c	2024-04-12 07:19:30.225431668 +0300
+++ freeciv/server/stdinhand.c	2024-04-12 07:19:45.481539337 +0300
@@ -615,6 +615,10 @@
   log_deprecation(_("/metamessage command is deprecated. "
                     "Set metamessage setting instead."));
 
+  if (is_longturn() && S_S_RUNNING == server_state()) {
+    return FALSE;
+  }
+
   if (check) {
     return TRUE;
   }
@@ -702,6 +706,10 @@
 {
   fc_assert_ret(pplayer != NULL);
 
+  if (is_longturn() && S_S_RUNNING == server_state()) {
+    return;
+  }
+
   if (is_human(pplayer)) {
     cmd_reply(CMD_AITOGGLE, caller, C_OK,
 	      _("%s is now under AI control."),
@@ -728,6 +736,10 @@
   enum m_pre_result match_result;
   struct player *pplayer;
 
+  if (is_longturn() && S_S_RUNNING == server_state()) {
+    return FALSE;
+  }
+
   pplayer = player_by_name_prefix(arg, &match_result);
 
   if (!pplayer) {
