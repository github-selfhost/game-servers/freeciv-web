# The Git SHA hash for the commit to checkout from
# https://github.com/freeciv/freeciv

FCREV=5db75379c8a9e6ecef75449047ddef065549a440

ORIGCAPSTR="+Freeciv.Devel-\${MAIN_VERSION}-2024.Aug.30"

# There's no need to bump this constantly as current freeciv-web
# makes no connections to outside world - all connections are
# internal to it. The server and client are always compatible.
WEBCAPSTR="+Freeciv.Web.Devel-3.3"
